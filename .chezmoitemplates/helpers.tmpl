#!/bin/sh

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

lowercase() {
  echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

e_header() {
  printf "\n${bold}${purple}==========  %s  ==========${reset}\n" "$@"
}

e_arrow() {
  printf "➜ %s\n" "$@"
}

e_success() {
  printf "${green}✔ %s${reset}\n" "$@"
}

e_error() {
  printf "${red}✖ %s${reset}\n" "$@"
}

e_warning() {
  printf "${tan}➜ %s${reset}\n" "$@"
}

e_underline() {
  printf "${underline}${bold}%s${reset}\n" "$@"
}

e_bold() {
  printf "${bold}%s${reset}\n" "$@"
}

e_note() {
  printf "${underline}${bold}${blue}Note:${reset}  ${yellow}%s${reset}\n" "$@"
}

is_confirmed() {
if [[ "$REPLY" =~ ^[Yy]$ ]]; then
  return 0
fi
  return 1
}

#brew install emacs iripgrep lftp curl broot php@7.4 notmuch asciinema msmtp bat awscli ranger rsync rclone cookiecutter mtr neofetch
#brew install shellcheck tealdeer terraforma caddy choose dnscrypt-proxy duf dust exa fd wtfutil wrk vegeta tree sd 
#brew install ansible davmail docker dstask ffmpeg gnupg hugo htop irssi isync neovim nmap opensc openssh pandoc pinentry-mac qemu 
#brew install the_silver_search tmux wget wireguard-tools fortune fzf gh git-delta glances google-cloud-sdk pandoc gopass hey
#brew install hping httpie jq lbdb inetutils thefuck
#brew tap homebrew/cask-drivers

############################### Comparing A List ###############################

# recipes=(
#   A-random-package
#   bash
#   Another-random-package
#   git
# )
# list="$(to_install "${recipes[*]}" "$(brew list)")"
# if [[ "$list" ]]; then
# for item in ${list[@]}
#   do
#     echo "$item is not on the list"
#   done
# else
# e_arrow "Nothing to install.  You've already got them all."
# fi

# USAGE FOR SEEKING CONFIRMATION
# seek_confirmation "Ask a question"
# Credit: https://github.com/kevva/dotfiles
#
#seek_confirmation() {
#  printf "\\n${bold}%s${reset}" "$@"
#  read -p " (y/n) " -n 1
#  printf "\\n"
#}

#
# Pushover Notifications
# Usage: pushover "Title Goes Here" "Message Goes Here"
# Credit: http://ryonsherman.blogspot.com/2012/10/shell-script-to-send-pushover.html
#
#pushover () {
#  PUSHOVERURL="https://api.pushover.net/1/messages.json"
#  API_KEY=$PUSHOVER_API_KEY
#  USER_KEY=$PUSHOVER_USER_KEY
#  DEVICE=$PUSHOVER_DEVICE
#
#  TITLE="${1}"
#  MESSAGE="${2}"
#
#  curl \
#  -F "token=${API_KEY}" \
#  -F "user=${USER_KEY}" \
#  -F "device=${DEVICE}" \
#  -F "title=${TITLE}" \
#  -F "message=${MESSAGE}" \
#  "${PUSHOVERURL}" > /dev/null 2>&1
#}

################################################################################
#
# Set Colors
#
# Use colors, but only if connected to a terminal, and that terminal
# supports them.
if which tput >/dev/null 2>&1; then
  ncolors=$(tput colors)
fi
if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
  bold="$(tput bold)"
  underline=$(tput sgr 0 1)
  reset="$(tput sgr0)"
  red="$(tput setaf 1)"
  green="$(tput setaf 2)"
  yellow="$(tput setaf 3)"
  blue="$(tput setaf 4)"
  purple=$(tput setaf 171)
  tan=$(tput setaf 3)
else
  bold=""
  underline=""
  reset=""
  red=""
  green=""
  yellow=""
  blue=""
  purple=""
  tan=""
fi

set_sudo() {
  if command_exists sudo; then
    if sudo -vn 2>&1 | grep 'password' > /dev/null; then
      SUDO_BIN=$(command -v sudo)
    else
      SUDO_BIN=""
    fi
  fi
}

set_sudo

{{ if (ne .chezmoi.os "darwin") -}}
check_packagemgr() {
  if command_exists yum; then
    PKG_BIN=$(command -v yum)
    PKG_CMD="$SUDO_CMD $PKG_BIN -y -qq install"
  elif command_exists pkg; then
    PKG_BIN=$(command -v pkg)
    PKG_CMD="$SUDO_CMD $PKG_BIN install -y"
  elif command_exists pkg_add; then
    PKG_BIN=$(command -v pkg_add)
    PKG_CMD="$SUDO_CMD $PKG_BIN -U -I -x"
  elif command_exists apt; then
    PKG_BIN=$(command -v apt)
    PKG_CMD="$SUDO_CMD $PKG_BIN -y install"
  fi
}
{{ end }}

set_download() {
  if command_exists curl; then
    DOWNLOAD_BIN=$(command -v curl)
  elif command_exists wget; then
    DOWNLOAD_BIN=$(command -v wget)
  elif command_exists fetch; then
    DOWNLOAD_BIN=$(command -v fetch)
  elif command_exists curlie; then
    DOWNLOAD_BIN=$(command -v curlie)
  fi
}

set_download
{{ if (ne .chezmoi.os "darwin") -}}
check_packagemgr
{{ end }}

GIT_BIN=$(command -v git)
ZSH_BIN=$(command -v zsh)
NVIM_BIN=$(command -v nvim)
NVIM_VERSION="0.5.1"
if command_exists python3; then
  PYTHON_BIN="python3"
elif command_exists python3.6; then
  PYTHON_BIN="python3.6"
elif command_exists python3.7; then
  PYTHON_BIN="python3.7"
elif command_exists python3.8; then
  PYTHON_BIN="python3.8"
elif command_exists python; then
  PYTHON_BIN="python"
elif command_exists python2; then
  PYTHON_BIN="python2"
fi
FREEBSD_PY_DEFAULT="py38-"

# vi: set ft=sh
