-- init.lua
local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn   -- to call Vim functions e.g. fn.bufnr()
local g = vim.g     -- a table to access global variables
local opt = vim.opt -- to opt.options
local wo = vim.wo   -- to wo.options

-- require newewr configs from nvim/lua folder
require("plugins")
require("options")
require("plugin-config")
require("autocommands")
require("keybindings")

-- Read Old Config
cmd('source ~/.config/nvim/not.staged.vim')
