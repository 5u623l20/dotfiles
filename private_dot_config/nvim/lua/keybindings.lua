-- need a map method to handle the different kinds of key maps
local function map(mode, combo, mapping, opts)
  local options = {noremap = true}
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, combo, mapping, options)
end

-- NvimTree keybindings
map('n', '<F2>',        ':NvimTreeToggle<CR>', {noremap = true})
map('n', '<leader>r',   ':NvimTreeRefresh<CR>', {noremap = true})
map('n', '<leader>n',   ':NvimTreeFindFile<CR>', {noremap = true})

-- ReMap leader to ' '
map('', '<Space>', '<Nop>', { noremap = true, silent = true })
--nnoremap <silent> <Leader><space> :nohlsearch<cr>

--Remap for dealing with word wrap
map('n', 'k', "v:count == 0 ? 'gk' : 'k'", { noremap = true, expr = true, silent = true })
map('n', 'j', "v:count == 0 ? 'gj' : 'j'", { noremap = true, expr = true, silent = true })

-- Y yank until the end of line  (note: this is now a default on master)
map('n', 'Y', 'y$', { noremap = true })

--Add leader shortcuts for telescope
map('n', '<leader><space>', [[<cmd>lua require('telescope.builtin').buffers()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sf', [[<cmd>lua require('telescope.builtin').find_files({previewer = false})<CR>]], { noremap = true, silent = true })
map('n', '<leader>sb', [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sh', [[<cmd>lua require('telescope.builtin').help_tags()<CR>]], { noremap = true, silent = true })
map('n', '<leader>st', [[<cmd>lua require('telescope.builtin').tags()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sd', [[<cmd>lua require('telescope.builtin').grep_string()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sp', [[<cmd>lua require('telescope.builtin').live_grep()<CR>]], { noremap = true, silent = true })
map('n', '<leader>so', [[<cmd>lua require('telescope.builtin').tags{ only_current_buffer = true }<CR>]], { noremap = true, silent = true })
map('n', '<leader>?',  [[<cmd>lua require('telescope.builtin').oldfiles()<CR>]], { noremap = true, silent = true })

-- Trouble keybindings
map('n', '<leader>xx',  [[<cmd>Trouble<CR>]], {silent = true, noremap = true})
map('n', '<leader>xw',  [[<cmd>Trouble lsp_workspace_diagnostics<CR>]], {silent = true, noremap = true})
map('n', '<leader>xd',  [[<cmd>Trouble lsp_document_diagnostics<CR>]], {silent = true, noremap = true})
map('n', '<leader>xl',  [[<cmd>Trouble loclist<CR>]], {silent = true, noremap = true})
map('n', '<leader>xq',  [[<cmd>Trouble quickfix<CR>]], {silent = true, noremap = true})
map('n', 'gR',          [[<cmd>Trouble lsp_references<CR>]], {silent = true, noremap = true})

--Search mappings: These will make it so that going to the next one in a
--search will center on the line it's found in.
--nnoremap n nzzzv
--nnoremap N Nzzzv
--
--*****************************************************************************
-- Abbreviations
--*****************************************************************************
--no one is really happy until you have this shortcuts
--cnoreabbrev W! w!
--cnoreabbrev Q! q!
--cnoreabbrev Qall! qall!
--cnoreabbrev Wq wq
--cnoreabbrev Wa wa
--cnoreabbrev wQ wq
--cnoreabbrev WQ wq
--cnoreabbrev W w
--cnoreabbrev Q q
--cnoreabbrev Qall qall
--
--terminal emulation
--nnoremap <silent> <leader>sh :terminal<CR>
--
--*****************************************************************************
--remove trailing whitespaces
--command! FixWhitespace :%s/\s\+$//e

--""*****************************************************************************
--""" Mappings
--""*****************************************************************************
--"
--""" Split
--"noremap <Leader>h :<C-u>split<CR>
--"noremap <Leader>v :<C-u>vsplit<CR>
--"
--""" Git
--"noremap <Leader>ga :Gwrite<CR>
--"noremap <Leader>gc :Git commit --verbose<CR>
--"noremap <Leader>gsh :Gpush<CR>
--"noremap <Leader>gll :Gpull<CR>
--"noremap <Leader>gs :Gstatus<CR>
--"noremap <Leader>gb :Gblame<CR>
--"noremap <Leader>gd :Gvdiff<CR>
--"noremap <Leader>gr :Gremove<CR>
--"
--"" session management
--"nnoremap <leader>so :OpenSession<Space>
--"nnoremap <leader>ss :SaveSession<Space>
--"nnoremap <leader>sd :DeleteSession<CR>
--"nnoremap <leader>sc :CloseSession<CR>
--"
--""" Tabs
--"nnoremap <Tab> gt
--"nnoremap <S-Tab> gT
--"nnoremap <silent> <S-t> :tabnew<CR>
--"
--""" Set working directory
--"nnoremap <leader>. :lcd %:p:h<CR>
--"
--""" Opens an edit command with the path of the currently edited file filled in
--"noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>
--"
--""" Opens a tab edit command with the path of the currently edited file filled
--"noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>
--"
--"cnoremap <C-P> <C-R>=expand("%:p:h") . "/" <CR>
--"nnoremap <silent> <leader>b :Buffers<CR>
--"nnoremap <silent> <leader>e :FZF -m<CR>
--""Recovery commands from history through FZF
--"nmap <leader>y :History:<CR>
--"
--"" Tagbar
--"nmap <silent> <F4> :TagbarToggle<CR>
--"let g:tagbar_autofocus = 1
--"
--"" Disable visualbell
--"set noerrorbells visualbell t_vb=
--"if has('autocmd')
--"  autocmd GUIEnter * set visualbell t_vb=
--"endif
--"
--""" Copy/Paste/Cut
--"if has('unnamedplus')
--"  set clipboard^=unnamed,unnamedplus
--"endif
--"
--"noremap YY "+y<CR>
--"noremap <leader>p "+gP<CR>
--"noremap XX "+x<CR>
--"
--"if has('macunix')
--"  " pbcopy for OSX copy/paste
--"  vmap <C-x> :!pbcopy<CR>
--"  vmap <C-c> :w !pbcopy<CR><CR>
--"endif
--"
--""" Buffer nav
--"noremap <leader>z :bp<CR>
--"noremap <leader>q :bp<CR>
--"noremap <leader>x :bn<CR>
--"noremap <leader>w :bn<CR>
--"
--""" Close buffer
--"noremap <leader>c :bd<CR>
--"
--""" Clean search (highlight)
--"nnoremap <silent> <leader><space> :noh<cr>
--"
--""" Switching windows
--"noremap <C-j> <C-w>j
--"noremap <C-k> <C-w>k
--"noremap <C-l> <C-w>l
--"noremap <C-h> <C-w>h
--"
--""" Vmap for maintain Visual Mode after shifting > and <
--"vmap < <gv
--"vmap > >gv
--"
--""" Move visual block
--"vnoremap J :m '>+1<CR>gv=gv
--"vnoremap K :m '<-2<CR>gv=gv
--"
--""" Open current line on GitHub
--"nnoremap <Leader>o :.Gbrowse<CR>
--"nnoremap ; :
--"nnoremap q: <nop>
--"nnoremap Q @q
--"
--"" NERDCommenter
--"nmap <C-_> <Plug>NERDCommenterToggle
--"vmap <C-_> <Plug>NERDCommenterToggle<CR>gv
--"tnoremap <ESC> <C-\><C-n>
--"
--"" Start interactive EasyAlign in visual mode (e.g. vipga)
--"xmap ga <Plug>(EasyAlign)
--"
--"" Start interactive EasyAlign for a motion/text object (e.g. gaip)
--"nmap ga <Plug>(EasyAlign)
--"
--"" Reselect visual block after indent/outdent
--"vnoremap < <gv
--"vnoremap > >gv
--""*****************************************************************************
--""" Custom configs
--""*****************************************************************************
--"
--"" c
--"autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab
--"autocmd FileType cpp setlocal tabstop=4 shiftwidth=4 expandtab
--"
--"
--"" go
--"" vim-go
--"" run :GoBuild or :GoTestCompile based on the go file
--"function! s:build_go_files()
--"  let l:file = expand('%')
--"  if l:file =~# '^\f\+_test\.go$'
--"    call go#test#Test(0, 1)
--"  elseif l:file =~# '^\f\+\.go$'
--"    call go#cmd#Build(0)
--"  endif
--"endfunction
--"
--"let g:go_list_type = "quickfix"
--"let g:go_fmt_command = "goimports"
--"let g:go_fmt_fail_silently = 1
--"
--"let g:go_highlight_types = 1
--"let g:go_highlight_fields = 1
--"let g:go_highlight_functions = 1
--"let g:go_highlight_methods = 1
--"let g:go_highlight_operators = 1
--"let g:go_highlight_build_constraints = 1
--"let g:go_highlight_structs = 1
--"let g:go_highlight_generate_tags = 1
--"let g:go_highlight_space_tab_error = 0
--"let g:go_highlight_array_whitespace_error = 0
--"let g:go_highlight_trailing_whitespace_error = 0
--"let g:go_highlight_extra_types = 1
--"
--"autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
--"
--"augroup completion_preview_close
--"  autocmd!
--"  if v:version > 703 || v:version == 703 && has('patch598')
--"    autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
--"  endif
--"augroup END
--"
--"augroup go
--"
--"  au!
--"  au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
--"  au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
--"  au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
--"  au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
--"
--"  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)
--"  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
--"  au FileType go nmap <Leader>db <Plug>(go-doc-browser)
--"
--"  au FileType go nmap <leader>r  <Plug>(go-run)
--"  au FileType go nmap <leader>t  <Plug>(go-test)
--"  au FileType go nmap <Leader>gt <Plug>(go-coverage-toggle)
--"  au FileType go nmap <Leader>i <Plug>(go-info)
--"  au FileType go nmap <silent> <Leader>l <Plug>(go-metalinter)
--"  au FileType go nmap <C-g> :GoDecls<cr>
--"  au FileType go nmap <leader>dr :GoDeclsDir<cr>
--"  au FileType go imap <C-g> <esc>:<C-u>GoDecls<cr>
--"  au FileType go imap <leader>dr <esc>:<C-u>GoDeclsDir<cr>
--"  au FileType go nmap <leader>rb :<C-u>call <SID>build_go_files()<CR>
--"
--"augroup END
--"
--"" ale
--":call extend(g:ale_linters, {
--"    \"go": ['golint', 'go vet'], })
--"
--"
--"" haskell
--"let g:haskell_conceal_wide = 1
--"let g:haskell_multiline_strings = 1
--"let g:necoghc_enable_detailed_browse = 1
--"autocmd Filetype haskell setlocal omnifunc=necoghc#omnifunc
--"
--"
--"" html
--"" for html files, 2 spaces
--"autocmd Filetype html setlocal ts=2 sw=2 expandtab
--"
--"
--"" lisp
--"
--"
--"" lua
--"
--"
--"" perl
--"
--"
--"" php
--"" Phpactor plugin
--"" Include use statement
--"nmap <Leader>u :call phpactor#UseAdd()<CR>
--"" Invoke the context menu
--"nmap <Leader>mm :call phpactor#ContextMenu()<CR>
--"" Invoke the navigation menu
--"nmap <Leader>nn :call phpactor#Navigate()<CR>
--"" Goto definition of class or class member under the cursor
--"nmap <Leader>oo :call phpactor#GotoDefinition()<CR>
--"nmap <Leader>oh :call phpactor#GotoDefinitionHsplit()<CR>
--"nmap <Leader>ov :call phpactor#GotoDefinitionVsplit()<CR>
--"nmap <Leader>ot :call phpactor#GotoDefinitionTab()<CR>
--"" Show brief information about the symbol under the cursor
--"nmap <Leader>K :call phpactor#Hover()<CR>
--"" Transform the classes in the current file
--"nmap <Leader>tt :call phpactor#Transform()<CR>
--"" Generate a new class (replacing the current file)
--"nmap <Leader>cc :call phpactor#ClassNew()<CR>
--"" Extract expression (normal mode)
--"nmap <silent><Leader>ee :call phpactor#ExtractExpression(v:false)<CR>
--"" Extract expression from selection
--"vmap <silent><Leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<CR>
--"" Extract method from selection
--"vmap <silent><Leader>em :<C-U>call phpactor#ExtractMethod()<CR>
--"
--"
--"" python
--"" vim-python
--"augroup vimrc-python
--"  autocmd!
--"  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
--"      \ formatoptions+=croq softtabstop=4
--"      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
--"augroup END
--"
--"" jedi-vim
--"let g:jedi#popup_on_dot = 0
--"let g:jedi#goto_assignments_command = "<leader>g"
--"let g:jedi#goto_definitions_command = "<leader>d"
--"let g:jedi#documentation_command = "K"
--"let g:jedi#usages_command = "<leader>n"
--"let g:jedi#rename_command = "<leader>r"
--"let g:jedi#show_call_signatures = "0"
--"let g:jedi#completions_command = "<C-Space>"
--"let g:jedi#smart_auto_mappings = 0
--"
--"" ale
--":call extend(g:ale_linters, {
--"    \'python': ['flake8'], })
--"
--"" rust
--"" Vim racer
--"au FileType rust nmap gd <Plug>(rust-def)
--"au FileType rust nmap gs <Plug>(rust-def-split)
--"au FileType rust nmap gx <Plug>(rust-def-vertical)
--"au FileType rust nmap <leader>gd <Plug>(rust-doc)
