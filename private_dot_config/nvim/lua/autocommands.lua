-- autocommands
local api = vim.api  -- access vim api

function NVIM_CREATE_AUGROUPS(definitions)
  for group_name, definition in pairs(definitions) do
    api.nvim_command('augroup '..group_name)
    api.nvim_command('autocmd!')
    for _, def in ipairs(definition) do
      local command = table.concat(vim.tbl_flatten{'autocmd', def}, ' ')
        api.nvim_command(command)
    end
    api.nvim_command('augroup END')
  end
end

local autocmds = {
  -- reload_vimrc = {
    -- Reload vim config automatically
    -- {"BufWritePost",[[$VIM_PATH/{*.vim,*.yaml,vimrc} nested source $MYVIMRC | redraw]]};
    -- {"BufWritePre", "$MYVIMRC", "lua ReloadConfig()"};
  -- };
  packer = {
    { "BufWritePost", "plugins.lua", "PackerCompile" };
  };
  terminal_job = {
    { "TermOpen", "*", [[tnoremap <buffer> <Esc> <c-\><c-n>]] };
    { "TermOpen", "*", "startinsert" };
    { "TermOpen", "*", "setlocal listchars= nonumber norelativenumber" };
  };
  restore_cursor = {
    { 'BufRead', '*', [[call setpos(".", getpos("'\""))]] };
  };
  save_shada = {
    {"VimLeave", "*", "wshada!"};
  };
  resize_windows_proportionally = {
    { "VimResized", "*", ":wincmd =" };
  };
  toggle_search_highlighting = {
    { "InsertEnter", "*", "setlocal nohlsearch" };
  };
  lua_highlight = {
    { "TextYankPost", "*", [[silent! lua vim.highlight.on_yank() {higroup="IncSearch", timeout=400}]] };
  };
  ansi_esc_log = {
    { "BufEnter", "*.log", ":AnsiEsc" };
  };
  YankHighlight = {
    { "TextYankPost", "*", [[silent! lua vim.highlight.on_yank() {higroup="IncSearch", timeout=400}]] };
  };
}

NVIM_CREATE_AUGROUPS(autocmds)
-- autocommands END

--*****************************************************************************
-- Commands
--*****************************************************************************
-- remove trailing whitespaces
--command! FixWhitespace :%s/\s\+$//e

--""*****************************************************************************
--""" Functions
--""*****************************************************************************
--if !exists('*s:setupWrapping')
--  function s:setupWrapping()
--    set wrap
--    set wm=2
--   set textwidth=79
--  endfunction
--endif
--
--"*****************************************************************************
--"" Autocmd Rules
--"*****************************************************************************
--"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
--augroup vimrc-sync-fromstart
--  autocmd!
--  autocmd BufEnter * :syntax sync maxlines=200
--augroup END
--
--"" Remember cursor position
--augroup vimrc-remember-cursor-position
--  autocmd!
--  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
--augroup END
--
--"" txt
--augroup vimrc-wrapping
--  autocmd!
--  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
--augroup END
--
--"" make/cmake
--augroup vimrc-make-cmake
--  autocmd!
--  autocmd FileType make setlocal noexpandtab
--  autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake
--augroup END
--
--""*****************************************************************************
--""" Mappings
--""*****************************************************************************
--"
--""" fzf.vim
--"set wildmode=list:longest,list:full
--"set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
--"let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"
--"
--"" The Silver Searcher
--"if executable('ag')
--"  let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
--"  set grepprg=ag\ --nogroup\ --nocolor
--"endif
--"
--"" ripgrep
--"if executable('rg')
--"  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
--"  set grepprg=rg\ --vimgrep
--"  command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
--"endif
--"
--"cnoremap <C-P> <C-R>=expand("%:p:h") . "/" <CR>
--"nnoremap <silent> <leader>b :Buffers<CR>
--"nnoremap <silent> <leader>e :FZF -m<CR>
--""Recovery commands from history through FZF
--"nmap <leader>y :History:<CR>
--"
--"" snippets
--"let g:UltiSnipsExpandTrigger="<tab>"
--"let g:UltiSnipsJumpForwardTrigger="<tab>"
--"let g:UltiSnipsJumpBackwardTrigger="<c-b>"
--"let g:UltiSnipsEditSplit="vertical"
--"
--"" ale
--"let g:ale_linters = {}
--"
--"" Tagbar
--"nmap <silent> <F4> :TagbarToggle<CR>
--"let g:tagbar_autofocus = 1
--"
--"" Disable visualbell
--"set noerrorbells visualbell t_vb=
--"if has('autocmd')
--"  autocmd GUIEnter * set visualbell t_vb=
--"endif
--"
--""" Copy/Paste/Cut
--"if has('unnamedplus')
--"  set clipboard^=unnamed,unnamedplus
--"endif
--"
--"noremap YY "+y<CR>
--"noremap <leader>p "+gP<CR>
--"noremap XX "+x<CR>
--"
--"if has('macunix')
--"  " pbcopy for OSX copy/paste
--"  vmap <C-x> :!pbcopy<CR>
--"  vmap <C-c> :w !pbcopy<CR><CR>
--"endif
--"
--""" Buffer nav
--"noremap <leader>z :bp<CR>
--"noremap <leader>q :bp<CR>
--"noremap <leader>x :bn<CR>
--"noremap <leader>w :bn<CR>
--"
--""" Close buffer
--"noremap <leader>c :bd<CR>
--"
--""" Clean search (highlight)
--"nnoremap <silent> <leader><space> :noh<cr>
--"
--""" Switching windows
--"noremap <C-j> <C-w>j
--"noremap <C-k> <C-w>k
--"noremap <C-l> <C-w>l
--"noremap <C-h> <C-w>h
--"
--""" Vmap for maintain Visual Mode after shifting > and <
--"vmap < <gv
--"vmap > >gv
--"
--""" Move visual block
--"vnoremap J :m '>+1<CR>gv=gv
--"vnoremap K :m '<-2<CR>gv=gv
--"
--""" Open current line on GitHub
--"nnoremap <Leader>o :.Gbrowse<CR>
--"nnoremap ; :
--"nnoremap q: <nop>
--"nnoremap Q @q
--"
--"" NERDCommenter
--"nmap <C-_> <Plug>NERDCommenterToggle
--"vmap <C-_> <Plug>NERDCommenterToggle<CR>gv
--"tnoremap <ESC> <C-\><C-n>
--"
--"" Start interactive EasyAlign in visual mode (e.g. vipga)
--"xmap ga <Plug>(EasyAlign)
--"
--"" Start interactive EasyAlign for a motion/text object (e.g. gaip)
--"nmap ga <Plug>(EasyAlign)
--"
--"" Reselect visual block after indent/outdent
--"vnoremap < <gv
--"vnoremap > >gv
--""*****************************************************************************
--""" Custom configs
--""*****************************************************************************
--"
--"" c
--"autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab
--"autocmd FileType cpp setlocal tabstop=4 shiftwidth=4 expandtab
--"
--"
--"" go
--"" vim-go
--"" run :GoBuild or :GoTestCompile based on the go file
--"function! s:build_go_files()
--"  let l:file = expand('%')
--"  if l:file =~# '^\f\+_test\.go$'
--"    call go#test#Test(0, 1)
--"  elseif l:file =~# '^\f\+\.go$'
--"    call go#cmd#Build(0)
--"  endif
--"endfunction
--"
--"let g:go_list_type = "quickfix"
--"let g:go_fmt_command = "goimports"
--"let g:go_fmt_fail_silently = 1
--"
--"let g:go_highlight_types = 1
--"let g:go_highlight_fields = 1
--"let g:go_highlight_functions = 1
--"let g:go_highlight_methods = 1
--"let g:go_highlight_operators = 1
--"let g:go_highlight_build_constraints = 1
--"let g:go_highlight_structs = 1
--"let g:go_highlight_generate_tags = 1
--"let g:go_highlight_space_tab_error = 0
--"let g:go_highlight_array_whitespace_error = 0
--"let g:go_highlight_trailing_whitespace_error = 0
--"let g:go_highlight_extra_types = 1
--"
--"autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
--"
--"augroup completion_preview_close
--"  autocmd!
--"  if v:version > 703 || v:version == 703 && has('patch598')
--"    autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
--"  endif
--"augroup END
--"
--"augroup go
--"
--"  au!
--"  au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
--"  au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
--"  au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
--"  au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
--"
--"  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)
--"  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
--"  au FileType go nmap <Leader>db <Plug>(go-doc-browser)
--"
--"  au FileType go nmap <leader>r  <Plug>(go-run)
--"  au FileType go nmap <leader>t  <Plug>(go-test)
--"  au FileType go nmap <Leader>gt <Plug>(go-coverage-toggle)
--"  au FileType go nmap <Leader>i <Plug>(go-info)
--"  au FileType go nmap <silent> <Leader>l <Plug>(go-metalinter)
--"  au FileType go nmap <C-g> :GoDecls<cr>
--"  au FileType go nmap <leader>dr :GoDeclsDir<cr>
--"  au FileType go imap <C-g> <esc>:<C-u>GoDecls<cr>
--"  au FileType go imap <leader>dr <esc>:<C-u>GoDeclsDir<cr>
--"  au FileType go nmap <leader>rb :<C-u>call <SID>build_go_files()<CR>
--"
--"augroup END
--"
--"" ale
--":call extend(g:ale_linters, {
--"    \"go": ['golint', 'go vet'], })
--"
--"
--"" haskell
--"autocmd Filetype haskell setlocal omnifunc=necoghc#omnifunc
--"
--"
--"" html
--"" for html files, 2 spaces
--"autocmd Filetype html setlocal ts=2 sw=2 expandtab
--"
--"
--"" python
--"" vim-python
--"augroup vimrc-python
--"  autocmd!
--"  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
--"      \ formatoptions+=croq softtabstop=4
--"      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
--"augroup END
--"
--"" ale
--":call extend(g:ale_linters, {
--"    \'python': ['flake8'], })
--"
--"" rust
--"" Vim racer
--"au FileType rust nmap gd <Plug>(rust-def)
--"au FileType rust nmap gs <Plug>(rust-def-split)
--"au FileType rust nmap gx <Plug>(rust-def-vertical)
--"au FileType rust nmap <leader>gd <Plug>(rust-doc)
