-- This file can be loaded by calling `require('plugins')` from your init.vim

-- Install packer
local fn = vim.fn
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  local packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

vim.api.nvim_exec(
  [[
  augroup Packer
    autocmd!
    autocmd BufWritePost init.lua PackerCompile
  augroup end
]],
  false
)

local use = require('packer').use
return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Simple plugins can be specified as strings
  use 'olimorris/onedarkpro.nvim'
  use 'norcalli/nvim-colorizer.lua'
  use 'windwp/nvim-autopairs'
  -- Add indentation guides even on blank lines
  use 'lukas-reineke/indent-blankline.nvim'

  -- LSP installer
  use 'neovim/nvim-lspconfig' -- Collection of configurations for built-in LSP client
  use 'williamboman/nvim-lsp-installer'

-- Autocompletion
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'

-- Snippets
  use 'dcampos/nvim-snippy' -- Snippets plugin
  use 'dcampos/cmp-snippy'  -- Snippets source for nvim-cmp
  use 'onsails/lspkind-nvim'-- Pictogram plugin for nvim-cmp

  -- File Explorer
  use {
    'kyazdani42/nvim-tree.lua',
    requires = 'kyazdani42/nvim-web-devicons',
    config = function() require'nvim-tree'.setup {} end
  }

  -- StatusLine
  use {
     'NTBBloodbath/galaxyline.nvim',
     config = function()
       require("galaxyline.themes.neonline")
     end,
     -- some optional icons
     requires = { "kyazdani42/nvim-web-devicons", opt = true }
  }

  -- Comments
  use {
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup()
    end
  }

  -- Lint engine
  use 'mfussenegger/nvim-lint'

  -- Diagnostic List
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      require("trouble").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
    end
  }

-- which-key
  use {
    "folke/which-key.nvim",
    config = function()
      require("which-key").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
      }
    end
  }
  -- Lazy loading:
  -- Load on specific commands
  -- use {'tpope/vim-dispatch', opt = true, cmd = {'Dispatch', 'Make', 'Focus', 'Start'}}

  -- Load on an autocommand event
  -- use {'andymass/vim-matchup', event = 'VimEnter'}

  -- Load on a combination of conditions: specific filetypes or commands
  -- Also run code after load (see the "config" key)
  -- use {
  --    'dense-analysis/ale'
  --   ft = {'sh', 'zsh', 'bash', 'c', 'cpp', 'cmake', 'html', 'markdown', 'racket', 'vim', 'tex'},
  --   cmd = 'ALEEnable',
  --   config = 'vim.cmd[[ALEEnable]]'
  -- }

  -- Plugins can have dependencies on other plugins
  -- use {
  --   'haorenW1025/completion-nvim',
  --   opt = true,
  --   requires = {{'hrsh7th/vim-vsnip', opt = true}, {'hrsh7th/vim-vsnip-integ', opt = true}}
  -- }

  -- Plugins can also depend on rocks from luarocks.org:
  -- use {
  --   'my/supercoolplugin',
  --   rocks = {'lpeg', {'lua-cjson', version = '2.1.0'}}
  -- }

  -- Plugins can have post-install/update hooks
  -- use {'iamcco/markdown-preview.nvim', run = 'cd app && yarn install', cmd = 'MarkdownPreview'}

-- Highlight, edit, and navigate code using a fast incremental parsing library
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  -- Additional textobjects for treesitter
  use 'nvim-treesitter/nvim-treesitter-textobjects'

  -- Post-install/update hook with call of vimscript function with argument
  -- use { 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end }

--use 'airblade/vim-gitgutter'
--use 'vim-scripts/grep.vim'
--use 'Raimondi/delimitMate'
--use 'majutsushi/tagbar'
--use 'editor-bootstrap/vim-bootstrap-updater'

--use 'junegunn/fzf.vim'
--let g:make = 'gmake'
--if exists('make')
--        let g:make = 'make'
--endif
--use { 'Shougo/vimproc.vim', {'do': g:make} }

-- Vim-Session
--use 'xolox/vim-misc'
--use 'xolox/vim-session'

-- Snippets
--use 'honza/vim-snippets'

-- c
--use { 'vim-scripts/c.vim', {'for': ['c', 'cpp']} }
--use 'ludwig/split-manpage.vim'

-- go
-- Go Lang Bundle
-- use { 'fatih/vim-go', {'on_ft': 'go'} }

-- haskell
-- Haskell Bundle
--use 'eagletmt/neco-ghc'
--use 'dag/vim2hs'
--use 'pbrisbin/vim-syntax-shakespeare'

-- html
-- HTML Bundle
--use 'hail2u/vim-css3-syntax'
--use 'tpope/vim-haml'
--use 'mattn/emmet-vim'

-- lisp
-- Lisp Bundle
--use 'vim-scripts/slimv.vim'

-- lua
-- Lua Bundle
--use 'xolox/vim-lua-ftplugin'
--use 'xolox/vim-lua-inspect'

-- perl
-- Perl Bundle
--use 'vim-perl/vim-perl'
--use 'c9s/perlomni.vim'

-- php
-- PHP Bundle
--use { 'phpactor/phpactor', {'for': 'php', 'do': 'composer install --no-dev -o'} }
--use 'stephpy/vim-php-cs-fixer'

-- python
-- Python Bundle
--use 'davidhalter/jedi-vim'
--use { 'raimon49/requirements.txt.vim', {'for': 'requirements'} }

-- rust
-- Vim racer
--use 'racer-rust/vim-racer'

-- Rust.vim
--use 'rust-lang/rust.vim'

-- Async.vim
--use 'prabirshrestha/async.vim'

-- Vim lsp
--use 'prabirshrestha/vim-lsp'

-- Asyncomplete.vim
--use 'prabirshrestha/asyncomplete.vim'

-- Asyncomplete lsp.vim
--use 'prabirshrestha/asyncomplete-lsp.vim'

-- scala
--if has('python')
--     sbt-vim
--    use 'ktvoelker/sbt-vim'
--endif
-- vim-scala
--use 'derekwyatt/vim-scala'

-- Include user's extra bundle
-- use 'editorconfig/editorconfig-vim'
-- use { 'bronson/vim-trailing-whitespace', {'on_cmd': 'FixWhitespace'} }
-- use { 'jamessan/vim-gnupg', {'on_ft': 'asc'} }
-- use { 'jremmen/vim-ripgrep', {'on_cmd': 'Rg'} }
-- use 'junegunn/vim-easy-align'
-- use 'sheerun/vim-polyglot'
-- use 'tpope/vim-repeat'
-- use 'tpope/vim-surround'
  use 'tpope/vim-fugitive' -- Git commands in nvim
  use 'tpope/vim-rhubarb' -- Fugitive-companion to interact with github
  -- UI to select things (files, grep results, open buffers...)
  use { 'nvim-telescope/telescope.nvim', requires = { 'nvim-lua/plenary.nvim' } }
  -- Add git related info in the signs columns and popups
  use { 'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' } }

  if packer_bootstrap then
    require('packer').sync()
  end
end)
