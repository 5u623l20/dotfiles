local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn   -- to call Vim functions e.g. fn.bufnr()
local g = vim.g     -- a table to access global variables
local opt = vim.opt -- to opt.options
local wo = vim.wo   -- to wo.options
--*****************************************************************************
-- Basic Setup
--*****************************************************************************
--Incremental live completion (note: this is now a default on master)
opt.inccommand = 'nosplit'

-- Encoding
opt.encoding = 'utf-8'
opt.fileencoding= 'utf-8'
opt.fileencodings= 'utf-8'
opt.ttyfast = true

-- Fix backspace indent
opt.backspace= {'indent', 'eol', 'start'}

--Enable break indent
opt.breakindent = true

--Save undo history
opt.undofile = true

-- Tabs. May be overridden by autocmd rules
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 2
opt.expandtab = true

-- Map leader to ' '
g.mapleader = ' '
g.maplocalleader = ','

-- Enable hidden buffers
opt.hidden = true

-- Searching
-- Set highlight on search
opt.hlsearch = true
opt.incsearch = true
-- Case insensitive searching UNLESS /C or capital in search
opt.ignorecase = true
opt.smartcase = true
opt.showmatch = true
opt.wildmode = { 'longest', 'list' }
opt.wildignore = { '*/.idea/*', '*/.git/*', '*/.svn/*', '*/vendor/*', '*/node_modules/*', '*/tmp/*', '*.so', '*.swp', '*.zip', '*.pyc', '*.db', '*.sqlite', '*.o', '*.obj', '*.rbc', '__pycache__' } 

opt.fileformat = 'unix'
opt.fileformats = {'unix', 'mac' }

if os.getenv('SHELL') ~= nil then
  opt.shell = os.getenv('SHELL')
else
  opt.shell = '/bin/sh'
end

-- session management - REVIEW
g.session_directory = '~/.config/nvim/session'
g.session_autoload = 'no'
g.session_autosave = 'no'
g.session_command_aliases = true

-- Visual Settings
cmd('syntax enable')
opt.number = true
opt.relativenumber = true

g.no_buffers_menu = true
--if $TERM =~ '^\(vte\|gnome\)\(-.*\)\?$'
if (os.getenv('TMUX') ~= nil or os.getenv('COLORTERM') == 'truecolor' or string.find(os.getenv('TERM'), '^xterm') ~= nil or string.find(os.getenv('TERM'), '^tmux') ~= nil ) then
  opt.termguicolors = true
  opt.background = 'dark'
  require('onedarkpro').load()
  require'colorizer'.setup()
end

opt.mousemodel = 'popup'
opt.gfn = 'Monospace:h10'

-- g.CSApprox_loaded = true

-- Disable the blinking cursor.
opt.gcr = 'a:blinkon0'

opt.scrolloff = 5

-- Status bar
opt.laststatus = 2

-- Use modeline overrides
opt.modeline = true
opt.modelines = 1

opt.title = true
opt.titleold = 'Terminal'
opt.titlestring = '%F'

-- Some basic globals for snippets
g.snips_author = "Muhammad Moinur Rahman"
--if exists("*fugitive#statusline")
--  opt.statusline+=%{fugitive#statusline()}
--endif

--*****************************************************************************
-- Abbreviations
--*****************************************************************************
-- no one is really happy until you have this shortcuts
--cnoreabbrev W! w!
--cnoreabbrev Q! q!
--cnoreabbrev Qall! qall!
--cnoreabbrev Wq wq
--cnoreabbrev Wa wa
--cnoreabbrev wQ wq
--cnoreabbrev WQ wq
--cnoreabbrev W w
--cnoreabbrev Q q
--cnoreabbrev Qall qall
--
--
-- grep.vim
--let Grep_Default_Options = '-IR'
--let Grep_Skip_Files = '*.log *.db'
--let Grep_Skip_Dirs = '.git node_modules'

-- terminal emulation
--nnoremap <silent> <leader>sh :terminal<CR>

--*****************************************************************************
-- Commands
--*****************************************************************************
-- remove trailing whitespaces
--command! FixWhitespace :%s/\s\+$//e
--
--*****************************************************************************
-- Functions
--*****************************************************************************
--if !exists('*s:setupWrapping')
--  function s:setupWrapping()
--    opt.wrap
--    opt.wm=2
--   opt.textwidth=79
--  endfunction
--endif

--*****************************************************************************
-- Autocmd Rules
--****************************************************************************
-- The PC is fast enough, do syntax highlight syncing from start unless 200 lines
--augroup vimrc-sync-fromstart
--  autocmd!
--  autocmd BufEnter * :syntax sync maxlines=200
--augroup END

-- Remember cursor position
--augroup vimrc-remember-cursor-position
--  autocmd!
--  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
--augroup END
--
-- txt
--augroup vimrc-wrapping
--  autocmd!
--  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
--augroup END

-- make/cmake
--augroup vimrc-make-cmake
--  autocmd!
--  autocmd FileType make setlocal noexpandtab
--  autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake
--augroup END

opt.autoread = true
--
--*****************************************************************************
-- Mappings
--*****************************************************************************

-- fzf.vim
--let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

-- The Silver Searcher
--if executable('ag')
--  let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
--  opt.grepprg=ag\ --nogroup\ --nocolor
--endif
--
-- ripgrep
--if executable('rg')
--  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
--  opt.grepprg=rg\ --vimgrep
--  command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
--endif
--
-- snippets
--g.UltiSnipsExpandTrigger="<tab>"
--g.UltiSnipsJumpForwardTrigger="<tab>"
--g.UltiSnipsJumpBackwardTrigger="<c-b>"
--g.UltiSnipsEditSplit="vertical"

-- ale
--g.ale_linters = {}
--
-- Tagbar
--nmap <silent> <F4> :TagbarToggle<CR>
--g.tagbar_autofocus = 1
--
--"" Disable visualbell
--"opt.noerrorbells visualbell t_vb=
--"if has('autocmd')
--"  autocmd GUIEnter * opt.visualbell t_vb=
--"endif

-- Copy/Paste/Cut
if fn.has('unnamedplus') then
  opt.clipboard:prepend{'unnamed', 'unnamedplus'}
end

--"noremap YY "+y<CR>
--"noremap <leader>p "+gP<CR>
--"noremap XX "+x<CR>
--"
--"if has('macunix')
--"  " pbcopy for OSX copy/paste
--"  vmap <C-x> :!pbcopy<CR>
--"  vmap <C-c> :w !pbcopy<CR><CR>
--"endif
--"
--""" Buffer nav
--"noremap <leader>z :bp<CR>
--"noremap <leader>q :bp<CR>
--"noremap <leader>x :bn<CR>
--"noremap <leader>w :bn<CR>
--"
--""" Close buffer
--"noremap <leader>c :bd<CR>
--"
--""" Clean search (highlight)
--"nnoremap <silent> <leader><space> :noh<cr>
--"
--""" Switching windows
--"noremap <C-j> <C-w>j
--"noremap <C-k> <C-w>k
--"noremap <C-l> <C-w>l
--"noremap <C-h> <C-w>h
--"
--""" Vmap for maintain Visual Mode after shifting > and <
--"vmap < <gv
--"vmap > >gv
--"
--""" Move visual block
--"vnoremap J :m '>+1<CR>gv=gv
--"vnoremap K :m '<-2<CR>gv=gv
--"
--""" Open current line on GitHub
--"nnoremap <Leader>o :.Gbrowse<CR>
--"nnoremap ; :
--"nnoremap q: <nop>
--"nnoremap Q @q
--"
--"" NERDCommenter
--"nmap <C-_> <Plug>NERDCommenterToggle
--"vmap <C-_> <Plug>NERDCommenterToggle<CR>gv
--"tnoremap <ESC> <C-\><C-n>
--"
--"" Start interactive EasyAlign in visual mode (e.g. vipga)
--"xmap ga <Plug>(EasyAlign)
--"
--"" Start interactive EasyAlign for a motion/text object (e.g. gaip)
--"nmap ga <Plug>(EasyAlign)
--"
--"" Reselect visual block after indent/outdent
--"vnoremap < <gv
--"vnoremap > >gv
--""*****************************************************************************
--""" Custom configs
--""*****************************************************************************
--"
--"" c
--"autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab
--"autocmd FileType cpp setlocal tabstop=4 shiftwidth=4 expandtab
--"
--"
--"" go
--"" vim-go
--"" run :GoBuild or :GoTestCompile based on the go file
--"function! s:build_go_files()
--"  let l:file = expand('%')
--"  if l:file =~# '^\f\+_test\.go$'
--"    call go#test#Test(0, 1)
--"  elseif l:file =~# '^\f\+\.go$'
--"    call go#cmd#Build(0)
--"  endif
--"endfunction
--"
--"g.go_list_type = "quickfix"
--"g.go_fmt_command = "goimports"
--"g.go_fmt_fail_silently = 1
--"
--"g.go_highlight_types = 1
--"g.go_highlight_fields = 1
--"g.go_highlight_functions = 1
--"g.go_highlight_methods = 1
--"g.go_highlight_operators = 1
--"g.go_highlight_build_constraints = 1
--"g.go_highlight_structs = 1
--"g.go_highlight_generate_tags = 1
--"g.go_highlight_space_tab_error = 0
--"g.go_highlight_array_whitespace_error = 0
--"g.go_highlight_trailing_whitespace_error = 0
--"g.go_highlight_extra_types = 1
--"
--"autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
--"
--"augroup completion_preview_close
--"  autocmd!
--"  if v:version > 703 || v:version == 703 && has('patch598')
--"    autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
--"  endif
--"augroup END
--"
--"augroup go
--"
--"  au!
--"  au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
--"  au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
--"  au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
--"  au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
--"
--"  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)
--"  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
--"  au FileType go nmap <Leader>db <Plug>(go-doc-browser)
--"
--"  au FileType go nmap <leader>r  <Plug>(go-run)
--"  au FileType go nmap <leader>t  <Plug>(go-test)
--"  au FileType go nmap <Leader>gt <Plug>(go-coverage-toggle)
--"  au FileType go nmap <Leader>i <Plug>(go-info)
--"  au FileType go nmap <silent> <Leader>l <Plug>(go-metalinter)
--"  au FileType go nmap <C-g> :GoDecls<cr>
--"  au FileType go nmap <leader>dr :GoDeclsDir<cr>
--"  au FileType go imap <C-g> <esc>:<C-u>GoDecls<cr>
--"  au FileType go imap <leader>dr <esc>:<C-u>GoDeclsDir<cr>
--"  au FileType go nmap <leader>rb :<C-u>call <SID>build_go_files()<CR>
--"
--"augroup END
--"
--"" ale
--":call extend(g:ale_linters, {
--"    \"go": ['golint', 'go vet'], })
--"
--"
--"" haskell
--"g.haskell_conceal_wide = 1
--"g.haskell_multiline_strings = 1
--"g.necoghc_enable_detailed_browse = 1
--"autocmd Filetype haskell setlocal omnifunc=necoghc#omnifunc
--"
--"
--"" html
--"" for html files, 2 spaces
--"autocmd Filetype html setlocal ts=2 sw=2 expandtab
--"
--"
--"" lisp
--"
--"
--"" perl
--"
--"
--"" php
--"" Phpactor plugin
--"" Include use statement
--"nmap <Leader>u :call phpactor#UseAdd()<CR>
--"" Invoke the context menu
--"nmap <Leader>mm :call phpactor#ContextMenu()<CR>
--"" Invoke the navigation menu
--"nmap <Leader>nn :call phpactor#Navigate()<CR>
--"" Goto definition of class or class member under the cursor
--"nmap <Leader>oo :call phpactor#GotoDefinition()<CR>
--"nmap <Leader>oh :call phpactor#GotoDefinitionHsplit()<CR>
--"nmap <Leader>ov :call phpactor#GotoDefinitionVsplit()<CR>
--"nmap <Leader>ot :call phpactor#GotoDefinitionTab()<CR>
--"" Show brief information about the symbol under the cursor
--"nmap <Leader>K :call phpactor#Hover()<CR>
--"" Transform the classes in the current file
--"nmap <Leader>tt :call phpactor#Transform()<CR>
--"" Generate a new class (replacing the current file)
--"nmap <Leader>cc :call phpactor#ClassNew()<CR>
--"" Extract expression (normal mode)
--"nmap <silent><Leader>ee :call phpactor#ExtractExpression(v:false)<CR>
--"" Extract expression from selection
--"vmap <silent><Leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<CR>
--"" Extract method from selection
--"vmap <silent><Leader>em :<C-U>call phpactor#ExtractMethod()<CR>
--"
--"
--"" python
--"" vim-python
--"augroup vimrc-python
--"  autocmd!
--"  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
--"      \ formatoptions+=croq softtabstop=4
--"      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
--"augroup END
--"
--"" jedi-vim
--"g.jedi#popup_on_dot = 0
--"g.jedi#goto_assignments_command = "<leader>g"
--"g.jedi#goto_definitions_command = "<leader>d"
--"g.jedi#documentation_command = "K"
--"g.jedi#usages_command = "<leader>n"
--"g.jedi#rename_command = "<leader>r"
--"g.jedi#show_call_signatures = "0"
--"g.jedi#completions_command = "<C-Space>"
--"g.jedi#smart_auto_mappings = 0
--
-- ale
--:call extend(g:ale_linters, {
--    \'python': ['flake8'], })
--
-- Syntax highlight
--let python_highlight_all = 1

-- rust
-- Vim racer
--au FileType rust nmap gd <Plug>(rust-def)
--au FileType rust nmap gs <Plug>(rust-def-split)
--au FileType rust nmap gx <Plug>(rust-def-vertical)
--au FileType rust nmap <leader>gd <Plug>(rust-doc)

--*****************************************************************************
-- Convenience variables
--*****************************************************************************

opt.autoindent = true
-- Set completeopt to have a better completion experience
-- vim.o.completeopt = 'menuone,noselect'
opt.completeopt = {'menu', 'longest', 'menuone', 'noselect'}
opt.foldlevelstart = 20
opt.foldmethod = 'expr'
opt.foldexpr = 'nvim_treesitter#foldexpr()'
opt.linebreak = true
opt.mouse = 'a'
opt.backup = false
opt.list = true
opt.swapfile = false
opt.wrap = false
opt.writebackup = false
opt.path:append {"**"}
opt.shortmess = opt.shortmess
  + 'c'
opt.sidescrolloff = 5
opt.smartindent = true
opt.splitbelow = true
opt.splitright = true
opt.synmaxcol = 500
opt.textwidth=0
opt.timeoutlen = 300
-- Decrease update time
opt.updatetime = 300
opt.wrapmargin = 0

g.netrw_altv = 1
g.netrw_banner = 0
g.netrw_browse_split = 4
g.netrw_liststyle = 3
g.netrw_winsize = 20

-- Always show the signcolumn, otherwise it would shift the text each time
-- diagnostics appear/become resolved.
wo.signcolumn = 'yes'
