local cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
-- local fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
-- local g = vim.g      -- a table to access global variables
local opt = vim.opt  -- to opt.options

-- Load nvim-web-devicons
require'nvim-web-devicons'.setup {
  -- your personnal icons can go here (to override)
  -- DevIcon will be appended to `name`
  -- override = {
  --   zsh = {
  --     icon = "",
  --     color = "#428850",
  --     name = "Zsh"
  --   }
  -- };
  -- globally enable default icons (default to false)
  -- will get overriden by `get_icons` option
  default = true;
}

-- Load indent-blankline
opt.listchars:append("eol:↴")
cmd [[highlight IndentBlanklineIndent1 guifg=#E06C75 gui=nocombine]]
cmd [[highlight IndentBlanklineIndent2 guifg=#E5C07B gui=nocombine]]
cmd [[highlight IndentBlanklineIndent3 guifg=#98C379 gui=nocombine]]
cmd [[highlight IndentBlanklineIndent4 guifg=#56B6C2 gui=nocombine]]
cmd [[highlight IndentBlanklineIndent5 guifg=#61AFEF gui=nocombine]]
cmd [[highlight IndentBlanklineIndent6 guifg=#C678DD gui=nocombine]]
require("indent_blankline").setup {
    char = "|",
    char_highlight = 'LineNr',
    show_end_of_line = true,
    show_trailing_blankline_indent = false,
    space_char_blankline = " ",
    char_highlight_list = {
        "IndentBlanklineIndent1",
        "IndentBlanklineIndent2",
        "IndentBlanklineIndent3",
        "IndentBlanklineIndent4",
        "IndentBlanklineIndent5",
        "IndentBlanklineIndent6",
    },
    filetype_exclude = { 'help', 'packer' },
    buftype_exclude = { 'terminal', 'nofile' }
}

-- Load nvim-lspconfig
local nvim_lsp = require('lspconfig')
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(_, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
  buf_set_keymap('n', '<leader>so', [[<cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>]], opts)
end

-- nvim-cmp supports additional completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'bashls', 'clangd', 'pyright', 'rust_analyzer', 'sumneko_lua', 'tsserver' }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
    flags = {
      debounce_text_changes = 150,
    }
  }
end

-- Load luasnip
-- local luasnip = require 'luasnip'
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end
-- Load nvim-snippy
local snippy = require("snippy")
snippy.setup({
    mappings = {
        is = {
            ["<Tab>"] = "expand_or_advance",
            ["<S-Tab>"] = "previous",
        },
        nx = {
            ["<leader>x"] = "cut_text",
        },
    },
})

-- Load nvim-cmp
local cmp = require'cmp'

-- Load lspkind-nvim
local lspkind = require('lspkind')

cmp.setup({
  snippet = {
    expand = function(args)
      -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
      require'snippy'.expand_snippet(args.body) -- For `snippy` users.
    end,
  },
  formatting = {
    format = lspkind.cmp_format({with_text = false, maxwidth = 50})
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<C-y>'] = cmp.config.disable, -- If you want to remove the default `<C-y>` mapping, You can specify `cmp.config.disable` value.
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif snippy.can_expand_or_advance() then
        snippy.expand_or_advance()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif snippy.can_jump(-1) then
        snippy.previous()
      else
        fallback()
      end
    end, { "i", "s" }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    -- { name = 'luasnip' }, -- For luasnip users.
    { name = 'snippy' }, -- For snippy users.
  }, {
    { name = 'buffer' },
  })
})

-- Use buffer source for `/`.
cmp.setup.cmdline('/', {
  sources = {
  { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':'.
cmp.setup.cmdline(':', {
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})

-- Load nvim-autopairs
require('nvim-autopairs').setup({
  disable_filetype = { "TelescopePrompt" , "vim" },
})
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))

local Rule = require('nvim-autopairs.rule')
local npairs = require("nvim-autopairs")

npairs.setup({
    check_ts = true,
    ts_config = {
        lua = {'string'},-- it will not add a pair on that treesitter node
        javascript = {'template_string'},
        java = false,-- don't check treesitter on java
    }
})

local ts_conds = require('nvim-autopairs.ts-conds')

-- press % => %% is only inside comment or string
npairs.add_rules({
  Rule("%", "%", "lua")
    :with_pair(ts_conds.is_ts_node({'string','comment'})),
  Rule("$", "$", "lua")
    :with_pair(ts_conds.is_not_ts_node({'function'}))
})

-- Setup lspconfig.
-- Custom server for sumneko_lsp
if os.getenv('DIST') == 'darwin' then
  local sumneko_root_path = vim.fn.getenv 'HOME' .. '/.local/share/nvim/lsp_servers/sumneko_lua/extension/server' -- Change to your sumneko root installation
  local sumneko_binary = sumneko_root_path .. '/bin/macOS/lua-language-server'

-- Make runtime files discoverable to the server
  local runtime_path = vim.split(package.path, ';')
  table.insert(runtime_path, 'lua/?.lua')
  table.insert(runtime_path, 'lua/?/init.lua')

  require('lspconfig').sumneko_lua.setup {
    cmd = { sumneko_binary, '-E', sumneko_root_path .. '/main.lua' },
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
      Lua = {
        runtime = {
          -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
          version = 'LuaJIT',
          -- Setup your lua path
          path = runtime_path,
        },
        diagnostics = {
          -- Get the language server to recognize the `vim` global
          globals = { 'vim' },
        },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = vim.api.nvim_get_runtime_file('', true),
        },
        -- Do not send telemetry data containing a randomized but unique identifier
        telemetry = {
          enable = false,
        },
      },
    },
  }
end

-- Treesitter configuration
-- Parsers must be installed manually via :TSInstall
require('nvim-treesitter.configs').setup {
  highlight = {
    enable = true, -- false will disable the whole extension
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = 'gnn',
      node_incremental = 'grn',
      scope_incremental = 'grc',
      node_decremental = 'grm',
    },
  },
  indent = {
    enable = true,
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
  },
}
-- Setup Telescope
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}

-- Setup Gitsigns
require('gitsigns').setup {
  signs = {
    add = { hl = 'GitGutterAdd', text = '+' },
    change = { hl = 'GitGutterChange', text = '~' },
    delete = { hl = 'GitGutterDelete', text = '_' },
    topdelete = { hl = 'GitGutterDelete', text = '‾' },
    changedelete = { hl = 'GitGutterChange', text = '~' },
  },
}

-- Setup comment
require('Comment').setup()

-- lspconfig-installer
local lsp_installer = require("nvim-lsp-installer")
local lsp_installer_servers = require'nvim-lsp-installer.servers'

local ok, rust_analyzer = lsp_installer_servers.get_server("rust_analyzer")
if ok then
    if not rust_analyzer:is_installed() then
        rust_analyzer:install()
    end
end
lsp_installer.on_server_ready(function (bashls) bashls:setup {} end)
-- grep.vim
--nnoremap <silent> <leader>f :Rgrep<CR>
--let Grep_Default_Options = '-IR'
--let Grep_Skip_Files = '*.log *.db'
--let Grep_Skip_Dirs = '.git node_modules'
--
-- terminal emulation
--nnoremap <silent> <leader>sh :terminal<CR>

--*****************************************************************************
-- Commands
--*****************************************************************************
-- remove trailing whitespaces
--command! FixWhitespace :%s/\s\+$//e
--
--*****************************************************************************
-- Functions
--*****************************************************************************
--if !exists('*s:setupWrapping')
--  function s:setupWrapping()
--    set wrap
--    set wm=2
--   set textwidth=79
--  endfunction
--endif
--
--*****************************************************************************
-- Autocmd Rules
--*****************************************************************************
-- The PC is fast enough, do syntax highlight syncing from start unless 200 lines
--augroup vimrc-sync-fromstart
--  autocmd!
--  autocmd BufEnter * :syntax sync maxlines=200
--augroup END
--
-- Remember cursor position
--augroup vimrc-remember-cursor-position
--  autocmd!
--  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
--augroup END
--
-- txt
--augroup vimrc-wrapping
--  autocmd!
--  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
--augroup END
--
-- make/cmake
--augroup vimrc-make-cmake
--  autocmd!
--  autocmd FileType make setlocal noexpandtab
--  autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake
--augroup END
--
--set autoread
--
--*****************************************************************************
-- Mappings
--*****************************************************************************
--
-- Split
--noremap <Leader>h :<C-u>split<CR>
--noremap <Leader>v :<C-u>vsplit<CR>
--
-- Git
--noremap <Leader>ga :Gwrite<CR>
--noremap <Leader>gc :Git commit --verbose<CR>
--noremap <Leader>gsh :Gpush<CR>
--noremap <Leader>gll :Gpull<CR>
--noremap <Leader>gs :Gstatus<CR>
--noremap <Leader>gb :Gblame<CR>
--noremap <Leader>gd :Gvdiff<CR>
--noremap <Leader>gr :Gremove<CR>
--
-- session management
--nnoremap <leader>so :OpenSession<Space>
--nnoremap <leader>ss :SaveSession<Space>
--nnoremap <leader>sd :DeleteSession<CR>
--nnoremap <leader>sc :CloseSession<CR>
--
-- Tabs
--nnoremap <Tab> gt
--nnoremap <S-Tab> gT
--nnoremap <silent> <S-t> :tabnew<CR>
--
-- Set working directory
--nnoremap <leader>. :lcd %:p:h<CR>
--
-- Opens an edit command with the path of the currently edited file filled in
--noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>
--
-- Opens a tab edit command with the path of the currently edited file filled
--noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>
--
-- fzf.vim
--set wildmode=list:longest,list:full
--set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
--let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"
--
-- The Silver Searcher
--if executable('ag')
--  let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
--  set grepprg=ag\ --nogroup\ --nocolor
--endif
--
-- ripgrep
--if executable('rg')
--  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
--  set grepprg=rg\ --vimgrep
--  command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
--endif
--
--cnoremap <C-P> <C-R>=expand("%:p:h") . "/" <CR>
--nnoremap <silent> <leader>b :Buffers<CR>
--nnoremap <silent> <leader>e :FZF -m<CR>
--Recovery commands from history through FZF
--nmap <leader>y :History:<CR>
--
-- snippets
--let g:UltiSnipsExpandTrigger="<tab>"
--let g:UltiSnipsJumpForwardTrigger="<tab>"
--let g:UltiSnipsJumpBackwardTrigger="<c-b>"
--let g:UltiSnipsEditSplit="vertical"
--
-- ale
--let g:ale_linters = {}
--
-- Tagbar
--nmap <silent> <F4> :TagbarToggle<CR>
--let g:tagbar_autofocus = 1
--
-- Disable visualbell
--set noerrorbells visualbell t_vb=
--if has('autocmd')
--  autocmd GUIEnter * set visualbell t_vb=
--endif
--
-- Copy/Paste/Cut
--if has('unnamedplus')
--  set clipboard^=unnamed,unnamedplus
--endif
--
--noremap YY "+y<CR>
--noremap <leader>p "+gP<CR>
--noremap XX "+x<CR>
--
--if has('macunix')
--  " pbcopy for OSX copy/paste
--  vmap <C-x> :!pbcopy<CR>
--  vmap <C-c> :w !pbcopy<CR><CR>
--endif
--
-- Buffer nav
--noremap <leader>z :bp<CR>
--noremap <leader>q :bp<CR>
--noremap <leader>x :bn<CR>
--noremap <leader>w :bn<CR>
--
-- Close buffer
--noremap <leader>c :bd<CR>
--
-- Clean search (highlight)
--nnoremap <silent> <leader><space> :noh<cr>
--
-- Switching windows
--noremap <C-j> <C-w>j
--noremap <C-k> <C-w>k
--noremap <C-l> <C-w>l
--noremap <C-h> <C-w>h
--
-- Vmap for maintain Visual Mode after shifting > and <
--vmap < <gv
--vmap > >gv
--
-- Move visual block
--vnoremap J :m '>+1<CR>gv=gv
--vnoremap K :m '<-2<CR>gv=gv
--
-- Open current line on GitHub
--nnoremap <Leader>o :.Gbrowse<CR>
--nnoremap ; :
--nnoremap q: <nop>
--nnoremap Q @q
--
-- NERDCommenter
--nmap <C-_> <Plug>NERDCommenterToggle
--vmap <C-_> <Plug>NERDCommenterToggle<CR>gv
--tnoremap <ESC> <C-\><C-n>
--
-- Start interactive EasyAlign in visual mode (e.g. vipga)
--xmap ga <Plug>(EasyAlign)
--
-- Start interactive EasyAlign for a motion/text object (e.g. gaip)
--nmap ga <Plug>(EasyAlign)
--
-- Reselect visual block after indent/outdent
--vnoremap < <gv
--vnoremap > >gv
--*****************************************************************************
-- Custom configs
--*****************************************************************************
--
-- c
--autocmd FileType c setlocal tabstop=4 shiftwidth=4 expandtab
--autocmd FileType cpp setlocal tabstop=4 shiftwidth=4 expandtab
--
--
-- go
-- vim-go
-- run :GoBuild or :GoTestCompile based on the go file
--function! s:build_go_files()
--  let l:file = expand('%')
--  if l:file =~# '^\f\+_test\.go$'
--    call go#test#Test(0, 1)
--  elseif l:file =~# '^\f\+\.go$'
--    call go#cmd#Build(0)
--  endif
--endfunction
--
--let g:go_list_type = "quickfix"
--let g:go_fmt_command = "goimports"
--let g:go_fmt_fail_silently = 1
--
--let g:go_highlight_types = 1
--let g:go_highlight_fields = 1
--let g:go_highlight_functions = 1
--let g:go_highlight_methods = 1
--let g:go_highlight_operators = 1
--let g:go_highlight_build_constraints = 1
--let g:go_highlight_structs = 1
--let g:go_highlight_generate_tags = 1
--let g:go_highlight_space_tab_error = 0
--let g:go_highlight_array_whitespace_error = 0
--let g:go_highlight_trailing_whitespace_error = 0
--let g:go_highlight_extra_types = 1
--
--autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
--
--augroup completion_preview_close
--  autocmd!
--  if v:version > 703 || v:version == 703 && has('patch598')
--    autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
--  endif
--augroup END
--
--augroup go
--
--  au!
--  au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
--  au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
--  au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
--  au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
--
--  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)
--  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
--  au FileType go nmap <Leader>db <Plug>(go-doc-browser)
--
--  au FileType go nmap <leader>r  <Plug>(go-run)
--  au FileType go nmap <leader>t  <Plug>(go-test)
--  au FileType go nmap <Leader>gt <Plug>(go-coverage-toggle)
--  au FileType go nmap <Leader>i <Plug>(go-info)
--  au FileType go nmap <silent> <Leader>l <Plug>(go-metalinter)
--  au FileType go nmap <C-g> :GoDecls<cr>
--  au FileType go nmap <leader>dr :GoDeclsDir<cr>
--  au FileType go imap <C-g> <esc>:<C-u>GoDecls<cr>
--  au FileType go imap <leader>dr <esc>:<C-u>GoDeclsDir<cr>
--  au FileType go nmap <leader>rb :<C-u>call <SID>build_go_files()<CR>
--
--augroup END
--
-- ale
--:call extend(g:ale_linters, {
--    \"go": ['golint', 'go vet'], })
--
-- haskell
--let g:haskell_conceal_wide = 1
--let g:haskell_multiline_strings = 1
--let g:necoghc_enable_detailed_browse = 1
--autocmd Filetype haskell setlocal omnifunc=necoghc#omnifunc
--
--
-- html
-- for html files, 2 spaces
--autocmd Filetype html setlocal ts=2 sw=2 expandtab
--
-- lisp
--
-- lua
--
-- perl
--
-- php
-- Phpactor plugin
-- Include use statement
--nmap <Leader>u :call phpactor#UseAdd()<CR>
-- Invoke the context menu
--nmap <Leader>mm :call phpactor#ContextMenu()<CR>
-- Invoke the navigation menu
--nmap <Leader>nn :call phpactor#Navigate()<CR>
-- Goto definition of class or class member under the cursor
--nmap <Leader>oo :call phpactor#GotoDefinition()<CR>
--nmap <Leader>oh :call phpactor#GotoDefinitionHsplit()<CR>
--nmap <Leader>ov :call phpactor#GotoDefinitionVsplit()<CR>
--nmap <Leader>ot :call phpactor#GotoDefinitionTab()<CR>
-- Show brief information about the symbol under the cursor
--nmap <Leader>K :call phpactor#Hover()<CR>
-- Transform the classes in the current file
--nmap <Leader>tt :call phpactor#Transform()<CR>
-- Generate a new class (replacing the current file)
--nmap <Leader>cc :call phpactor#ClassNew()<CR>
-- Extract expression (normal mode)
--nmap <silent><Leader>ee :call phpactor#ExtractExpression(v:false)<CR>
-- Extract expression from selection
--vmap <silent><Leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<CR>
-- Extract method from selection
--vmap <silent><Leader>em :<C-U>call phpactor#ExtractMethod()<CR>
--
-- python
-- vim-python
--augroup vimrc-python
--  autocmd!
--  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
--      \ formatoptions+=croq softtabstop=4
--      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
--augroup END
--
-- jedi-vim
--let g:jedi#popup_on_dot = 0
--let g:jedi#goto_assignments_command = "<leader>g"
--let g:jedi#goto_definitions_command = "<leader>d"
--let g:jedi#documentation_command = "K"
--let g:jedi#usages_command = "<leader>n"
--let g:jedi#rename_command = "<leader>r"
--let g:jedi#show_call_signatures = "0"
--let g:jedi#completions_command = "<C-Space>"
--let g:jedi#smart_auto_mappings = 0
--
-- ale
--:call extend(g:ale_linters, {
--    \'python': ['flake8'], })
--
-- vim-airline
--let g:airline#extensions#virtualenv#enabled = 1
--
-- Syntax highlight
--let python_highlight_all = 1
--
-- rust
-- Vim racer
--au FileType rust nmap gd <Plug>(rust-def)
--au FileType rust nmap gs <Plug>(rust-def-split)
--au FileType rust nmap gx <Plug>(rust-def-vertical)
--au FileType rust nmap <leader>gd <Plug>(rust-doc)
